/** Basic arithmetic operations */
const mylib = {
    /** Multiline arrow function */
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    /** Multiline arrow function*/
    substract: (a, b) => {
        return a - b;
    },
    /** Singleline arrow function (not anymore)*/
    divide: (divided, divisor) => {
        if (divisor === 0 ) {
            throw new Error('divisor 0 not allowed');
        }
        return divided / divisor;

    },
    /** Regulat function */
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = mylib;
