const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("My unit test", () => {
    before(()=> {
        // do something before the testing
    });
    it("Adds 2 + 2 and equals 4", () => {
        expect(mylib.add(2, 2)).to.equal(4);
    });
    it("Fails if divisor is 0", () => {
        expect(() => mylib.divide(9, 0)).to.throw();
    });
    after(() => {
        // do something after the testing
    });
});